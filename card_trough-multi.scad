include <peglib/defaults.scad>;
include <moslib/libchamfer.scad>;



nudge = 0.25;

tray_iw = 53 + nudge;
tray_ow = tray_iw + (wall_thickness * 2);

tray_il = 90 + nudge;
tray_ol = tray_il + (wall_thickness * 2);

tray_ih = 2 + nudge;
tray_oh = tray_ih + (wall_thickness * 1);


wall_chamfer = wall_thickness / 4;





module tray() {
    difference() {
        color("lightblue") {
          //  chamfered_box([tray_ow, tray_ol, wall_thickness], chamfer = wall_chamfer);
        }
        color("slategrey") {
            translate([0, tray_il / 2, 0]) {
          //  chamfered_box([tray_iw, base_size / 2, wall_thickness + 0.01], align = [0, -1, 0]);
            }
        }
    }

    translate([0, 0, 0]) {
        color("plum") {
            for(x = [-1, 1]) {
                translate([x * (tray_ow / 2), 0, 0]) {
                    chamfered_box([wall_thickness, tray_ol, wall_thickness], chamfer = wall_chamfer, align = [-x, 0, 0]);
                }
            }
            translate([0, (tray_ol / 2), 0]) {
                chamfered_box([tray_ow, wall_thickness, wall_thickness], chamfer = wall_chamfer, align = [0, -1, 0]);
            }
            translate([0, -(tray_ol / 2), 0]) {
                chamfered_box([tray_ow, tray_ol - base_size/2, wall_thickness], chamfer = wall_chamfer, align = [0, 1, 0]);
            }
        }
    }
        


    for(y = [-1, 1]) {
        translate([0, y * (tray_il / 2), 0]) {
            color("orange") {
                // end walls
                chamfered_box([tray_ow, wall_thickness, tray_oh], align = [0, y * 1, 1], chamfer = wall_chamfer);
            }
            color("red") {
                // end caps
                translate([0, 0, tray_oh]) {
                    chamfered_box([tray_ow, wall_thickness, wall_thickness], chamfer = wall_chamfer, align = [0, y * 0.5, -1]);
                }
            }
        }
    }
    
    for(x = [-1, 1]) {
        translate([x * (tray_iw / 2), 0]) {
            color("lavender") {
                // side walls
                chamfered_box([wall_thickness, tray_ol, tray_oh], align = [x * 1, 0, 1], chamfer = wall_chamfer);
            }
            color("peachpuff") {
                // side caps
                translate([0, 0, tray_oh]) {
                    chamfered_box([wall_thickness, tray_ol, wall_thickness], align = [x * 0.5, 0, -1], chamfer = wall_chamfer);
                }
            }
        }
    }
    

}

    offset = 9;


module trough() {


    difference() {
        translate([0, 0, -wall_thickness * 2]) {
            chamfered_box([tray_ow * 0.5, tray_ol + (wall_thickness * 4), 20], chamfer = 2.5, align = [0, 0, 1]);
        }

translate([-12.982, 0, 0]) {
        union() {
                                    foo = 1;

//            tray_ol = 200;
            translate([offset, 0, 0]) {
                rotate([0,-60,0]) {
                    color("red") {
                        gap = 2;
                        chamfered_box([tray_ow, tray_ol * foo + gap, tray_oh + (wall_thickness / 2) + gap], chamfer = wall_chamfer, align = [1, 0, 0]);
                    }
                }
            }

            translate([(tray_ow * 0.66 * 0.66) - offset, 0, 0]) {
                rotate([0, -60, 180]) {
                    color("red") {
                        gap = 2;
                        chamfered_box([tray_ow, tray_ol * foo + gap, tray_oh + (wall_thickness / 2) + gap], chamfer = wall_chamfer, align = [1, 0, 0]);
                    }
                }
                
            }
        }
    }
    }
}


module reference_cube(align = [0, 0, 1]) {
    color("lightgreen") {
        chamfered_box([tray_ow, tray_ol, tray_oh + (wall_thickness / 2)], chamfer = wall_chamfer, align = align);
    }
}


// true
// false

do_tray = false; 
do_reference_cube_t = false;


do_trough = true; 
do_reference_cube_tt = false;


// card tray
//
if (do_tray) { 
    tray();
}
if(do_reference_cube_t) {
    translate([0, 0, -wall_thickness /2]) {
        reference_cube([0,0,1]);
    }
}



// card tray tray
//
if(do_reference_cube_tt) {
                translate([offset, 0, 0]) {

    rotate([0,-60,0]) {
        rotate([0, 0, 90])
        translate([-(tray_ow / 2), -(tray_ol / 2), 0])
        reference_cube([1,0,0]);
    }
}
}
if(do_trough) {
        for(t = [0:6]) {
        echo(t);
        translate([tray_ow * 0.4 * t, 0, 0]) {
            trough();
        }
    }
}



//EOF
