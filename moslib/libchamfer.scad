//NOSTL
module chamfered_tube(height, od, id, align = [0, 0, 0], chamfer = 0, cutout = false, fn = 0) {

    $fn = fn ? fn : $fn;

    translate([(od / 2) * align.x, (od / 2) * align.y, (height / 2) * (align.z - 1)]) {

       difference() {
            rotate_extrude() {

                polygon(points = [
                           [id / 2          , chamfer],
                           [id / 2 + chamfer, 0      ],
                           [od / 2 - chamfer, 0      ],
                           [od / 2          , chamfer],
                           [od / 2          , height - chamfer],
                           [od / 2 - chamfer, height],
                           [id / 2 + chamfer, height],
                           [id / 2          , height - chamfer]
                           ]
                      );
            }


            cutout_version = 4;
            
            cutout = cutout ? cutout_version : false;

            if(cutout == 1) {
                color("steelblue") {
                    translate([0, -od/2, (height) / 2]) {
                        rotate([0, 90, 0]) {
                            $fn = 4;
                            hull() {
                                translate([-((height / 3) - (od / 4)), 0, 0]) {
                                    cylinder(h = height + 1, d = od / 2, center = true);
                                }
                                translate([((height / 3) - (od / 4)), 0, 0]) {
                                    cylinder(h = height + 1, d = od / 2, center = true);
                                }
                            }
                        }
                    }
                }
            }
            
            if(cutout == 2) {
                translate([0, 0, (height) / 4]) {
                    color("lightgreen") {
                        rotate([0, 0, -45])
                        rotate_extrude(angle = 180) 
                            polygon(points = [
                                [id/2 - 1, 0],
                                [od/2 + 1, 0], 
                                [od/2 + 1, height / 2],
                                [id/2 - 1, height / 2]
                            ]);
                    }
                }
                translate([0, 0, height / 2]) {
                    rotate([0, 90, 0]) {
                        color("red") {
                            intersection() {
                                rotate_extrude() 
                                    polygon(points = [
                                        [ 0,  0],
                                        [ 0, od],
                                        [od, od]
                                    ]);
                                cube([100, 100, 100]);
                            }
                        }
                    }
                }
            }
            
            if(cutout == 3) {
                $fn = 4;
                gapitude = 90;
                hull() {
                    color("red") {
                        translate([0, 0, height/6]) {
                            rotate([0, 90, 270 - (gapitude / 2)]) {
                                hull() {
                                    hull() {
                                        cylinder(d = 0.01, h = 0.01);
                                        translate([-height * (2/3), 0, 0]) {
                                            cylinder(d = 0.01, h = 0.01);
                                        }
                                    }
                                    translate([0, 0, od]) {
                                        hull() {
                                            translate([-height / 8, 0, 0]) {
                                                cylinder(d = height / 4, h = 0.01);
                                            }
                                            translate([-((height  * (2/3)) - (height / 8)), 0, 0]) {
                                                cylinder(d = height / 4, h = 0.01);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    color("green") {
                        translate([0, 0, height/6]) {
                            rotate([0, 90, 270 + (gapitude / 2)]) {
                                hull() {
                                    hull() {
                                        cylinder(d = 0.01, h = 0.01);
                                        translate([-height  * (2/3), 0, 0]) {
                                            cylinder(d = 0.01, h = 0.01);
                                        }
                                    }
                                    translate([0, 0, od]) {
                                        hull() {
                                            translate([-height / 8, 0, 0]) {
                                                cylinder(d = height / 4, h = 0.01);
                                            }
                                            translate([-((height  * (2/3)) - (height / 8)), 0, 0]) {
                                                cylinder(d = height / 4, h = 0.01);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
            }
            
            if(cutout == 4) {
                
                cutout_width = (1/3); // don't change the width - parameterizing of this is broken
                cutout_height = (2/3); 

                rotate([0, 0, 150 + ((1/2) * 360 * cutout_width)]) {  // this is broken, too (rotationally)

                    cor = (id / 2) + (((od - id) / 2) / 2);
                    icr = cor / 2;
                    ocr = cor * 2;
                    arclen = (2 * PI * cor) * cutout_width;


                    difference() {
                        color("salmon")
                        translate([0, 0, height / 2]) translate([0, 0, height * cutout_height / 2 * -1]) {
                            rotate_extrude(angle = 360 * cutout_width) {
                                translate([(id / 2) / 2, 0, 0]) {
                                    square([(2 * ((id / 2) / 2)) + (od / 2) - (id / 2), height * cutout_height]);
                                }
                            }
                        }

                        foo = sqrt((arclen / 4) * (arclen / 4) * 2);

                        translate([0, 0, height * (1/2)]) // start in the vertical center
                        rotate([0, 0, ((1/2) * 360 * cutout_width )]) // start in the rotational center
                        color("red") {

                                // ul
                                translate([0, arclen/2, 0])
                                translate([0, 0, height * cutout_height / 2]) // line up with top of pie
                                rotate([0, 0, -360 * cutout_width / 2]) // line up with edge of pie
                                rotate([135, 0, 0]) // stand on end
                                cube([od * 2, foo, foo]);

                                // ll
                                translate([0, arclen/2, 0])
                                translate([0, 0, -height * cutout_height / 2]) // line up with bottom of pie
                                rotate([0, 0, -360 * cutout_width / 2]) // line up with edge of pie
                                rotate([135, 0, 0]) // stand on end
                                cube([od * 2, foo, foo]);


                                // ur
                                translate([0, -arclen/2, 0])
                                translate([0, 0, height * cutout_height / 2]) // line up with top of pie
                                rotate([0, 0, 360 * cutout_width / 2]) // line up with edge of pie
                                rotate([-45, 0, 0]) // stand on end
                                cube([od * 2, foo, foo]);

                                // lr
                                translate([0, -arclen/2, 0])
                                translate([0, 0, - height * cutout_height / 2]) // line up with bottom of pie
                                rotate([0, 0, 360 * cutout_width / 2]) // line up with edge of pie
                                rotate([-45, 0, 0]) // stand on end
                                cube([od * 2, foo, foo]);

                        }

                    }
                }

            }


        }

    }
}



module chamfered_cylinder(height, od, align = [0, 0, 0], chamfer = 0, fn = 0) {

     hull() {
          chamfered_tube(height, od, 0, align, chamfer, fn = fn);
     }

}



module closed_chamfered_tube(height, od, id, align = [0, 0, 1], chamfer = 0, cutout = false, fn = 0) {

    chamfered_tube(height, od, id, align, chamfer, cutout, fn = fn);
    chamfered_cylinder((od - id) / 2, od - 0.01, align, chamfer, fn = fn);
    
}







module chamfered_box(dim = [0, 0, 0], align = [0, 0, 0], chamfer = 0, center = false) {
    _chamfer = chamfer;
    falign = center ? [0, 0, 0] : align;
    

    translate([(dim.x / 2) * falign.x, (dim.y / 2) * falign.y, (dim.z / 2) * falign.z]) {
        hull() {
            cube([dim.x                 , dim.y - (_chamfer * 2), dim.z - (_chamfer * 2)], center = true);
            cube([dim.x - (_chamfer * 2), dim.y                 , dim.z - (_chamfer * 2)], center = true);
            cube([dim.x - (_chamfer * 2), dim.y - (_chamfer * 2), dim.z                 ], center = true);
        }

    }
}


// true
// false

do_test_parts = false;


if(do_test_parts) {


    // box test in the LR quadrant
    //
    translate([20, -50, 0]) {
        chamfered_box(dim = [20, 20, 20], chamfer = 1);

        translate([30, 30, 0]) {
            chamfered_box(dim = [10, 20, 30], chamfer = 4);
        }
    }



    // tube tests in the UR quadrant
    //
    translate([50, 50, 0]) {

        translate([0, 100, 0]) {

            chamfered_tube(height = 10, od = 40, id = 30, chamfer = 2);
            chamfered_tube(height = 20, od = 60, id = 50, chamfer = 1);

            chamfered_cylinder(height = 20, od = 10, chamfer = 1);

            translate([0, 0, -30]) {
                closed_chamfered_tube(height = 40, od = 100, id = 80, chamfer = 3);
            }

        }


        translate([100, 0, 0]) {
            closed_chamfered_tube(height = 30, od = 60, id = 40, chamfer = 2);
        }

        translate([100, 100, 50]) {
            closed_chamfered_tube(height = 100, od = 80, id = 60, chamfer = 2, cutout = true);
        }


        translate([200, 0, 0]) {

            inch = 25.4;
            eighth = inch / 8;

            diameter_in_inches = 1;
            height_in_inches = 1;
            wall_thickness_in_eights = 1;


            closed_chamfered_tube(height = height_in_inches * inch, od = diameter_in_inches * inch, id = (diameter_in_inches * inch) - (wall_thickness_in_eights * eighth * 2), chamfer = 1);
        }

        chamfered_cylinder(height = 40, od = 30, chamfer = 5);

    }
}

// EOF
