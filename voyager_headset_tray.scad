include <peglib/defaults.scad>;
include <moslib/libchamfer.scad>;

include <peglib/defaults.scad>;
include <peglib/rail.scad>;


tray_iw = 70;
tray_ow = tray_iw + (wall_thickness * 2);

tray_il = 120;
tray_ol = tray_il + (wall_thickness * 2);

tray_ih = 10;
tray_oh = tray_ih + (wall_thickness * 2);

wall_chamfer = wall_thickness / 4;




module tray() {
        // tray
    //
    union() {
        translate([0, tray_ol / 2, wall_thickness / 2]) {
            color("lightblue") {
                chamfered_box(dim = [tray_ow, tray_ol, wall_thickness], chamfer = wall_chamfer);
            }
            difference(){
                union(){
                    color("salmon") {
                        for(x = [-1, 1]) {
                            hull() {
                                for(y = [-1, 1]) {
                                    translate([x * ((tray_iw + wall_thickness) / 2), y * ((tray_il + wall_thickness) / 2), 0]) {
                                        echo(x, y);
                                        chamfered_box([wall_thickness, wall_thickness, tray_oh + (((-y + 1) / 2 ) * (tray_oh * 2))], chamfer = wall_chamfer, align = [0, 0, 1]);
                                    }                    
                                }
                            }
                        }
                    }
                }
                translate([0, -65, -4])
                union() {
                    color("red") {
                        hull() {
                            translate([0, 15, 15]) {
                                rotate([45, 0, 0]) {
                                    cube([tray_ow + 1, 10, 10], center = true);
                                }
                            }
                            translate([0, 15, 25]) {
                                rotate([45, 0, 0]) {
                                    cube([tray_ow + 1, 10, 10], center = true);
                                }
                            }
                        }
                    }
                }
            }
            
            color("orange") {
                for(y = [-1, 1]) {
                    hull() {
                        for(x = [-1, 1]) {
                            translate([x * ((tray_iw + wall_thickness) / 2), y * ((tray_il + wall_thickness) / 2), 0]) {
                                echo(x, y);
                                chamfered_box([wall_thickness, wall_thickness, tray_oh+ (((-y + 1) / 2 ) * (tray_oh * 2))], chamfer = wall_chamfer, align = [0, 0, 1]);
                            }                    
                        }
                    }
                }
            }

        }
    }
    

}

    // usb: 15mmx7mm
    //


// true
// false

do_rail = true;
do_tray = true; 

if (do_rail) {
    translate([0, -0.01, 0]) {
        color("lightgreen") {
            rail(2, copies = 2, chamfer = wall_chamfer);
        }
    }
}    
    
if (do_tray) { 
    tray();
    
}

//EOF
