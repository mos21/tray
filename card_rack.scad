include <peglib/defaults.scad>;
include <moslib/libchamfer.scad>;



nudge = 0.5;

tray_iw = 53 + nudge;
tray_ow = tray_iw + (wall_thickness * 2);

tray_il = 90 + (nudge * 2);
tray_ol = tray_il + (wall_thickness * 2);

tray_ih = 2 + nudge;
tray_oh = tray_ih + (wall_thickness * 1.5);


wall_chamfer = wall_thickness / 4;




module reference_cube(align = [0, 0, 1]) {
    color("lightgreen") {
        chamfered_box([tray_ow, tray_ol, tray_oh], chamfer = wall_chamfer, align = align);
    }
}




// true
// false

do_reference_cube_h = false;

do_reference_cube_v = false;


if(do_reference_cube_h) {
    translate([0, 0, -wall_thickness /2]) {
        reference_cube([0,0,1]);
    }
}


if(do_reference_cube_v) {
        rotate([0, 90, 0]) {
                reference_cube(align = [0, 0, 3]);
        }
}


module frame() {
    color("pink") {
        chamfered_box([tray_oh, tray_ol + tray_oh * 2, tray_oh], align = [1, 0, 0], chamfer = tray_oh / 4);
        translate([tray_oh * 2, 0, 0]) {
            chamfered_box([tray_oh, tray_ol + tray_oh * 2, tray_oh], align = [1, 0, 0], chamfer = tray_oh / 4);
        }
    }


    color("skyblue") {
        translate([0, (tray_ol + tray_oh) / 2, 0]) {
            chamfered_box([tray_oh * 3, tray_oh, tray_oh], align = [1, 0, 0], chamfer = tray_oh / 4);
        }
        translate([0, -(tray_ol + tray_oh) / 2, 0]) {
            chamfered_box([tray_oh * 3, tray_oh, tray_oh], align = [1, 0, 0], chamfer = tray_oh / 4);
        }
    }
}

howmany = 10;

for (i = [0 : (howmany - 1)]) {
    echo(i);
    translate([i * (tray_oh * 2), 0, 0]) {
        frame();
    }
}


//EOF
