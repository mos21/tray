include <peglib/defaults.scad>;
include <peglib/rail.scad>;
include <moslib/libchamfer.scad>;



fn = 250; // target is fn = 250

pebble_diameter = 118;
tray_id = pebble_diameter;


module tray() {
    
    union() {
        color("coral") {
            closed_chamfered_tube(height = base_size, od = tray_id + (wall_thickness * 2), id = tray_id, align = [0, 1, 1], chamfer = wall_thickness / 4, cutout = false, fn = fn);

        }
    }
}
    


module hanger() {
    rail(copies = 3, chamfer = wall_thickness / 4, mind_the_gap = false, skip = 2);
}



module filler() {
    color("red") {
    
        difference() {  

           translate([0, wall_thickness * 2 + .21, 0]) {
                hull() {
                    hanger();
                    translate([0, wall_thickness * 2, 0]) {
                        hanger();
                    }
                }
            }

            hull() {
                resize([0, 0, base_size + 0.02], auto = [false, false, true])  {
                    translate([0, 0, -0.01]) {
                        tray();
                    }
                }
            }
        }

    }

}


module part() {
    tray();
    hanger();
    filler();
}

module clip() {
    resize([0, 0, base_size - 0.02], auto = [false, false, true])  {
        translate([0, 0, +0.01]) {
            chamfered_box([tray_id, tray_id, base_size], align = [1, 0, 1]);
        }
    }
}


// true
// false 

do_part = true;
do_clip = false;

difference() {

intersection() {

    if (do_clip) {
        clip();
    }


    if (do_part) {
        part();
    }

}

translate([0, wall_thickness, -1]) {
    $fn = fn;
    cylinder(d = base_size, h = base_size * 2);
    chamfered_box([base_size, base_size, base_size * 2], align = [0, -1, 1]);
}

}

//EOF
