include <peglib/defaults.scad>;
include <moslib/libchamfer.scad>;

include <peglib/defaults.scad>;
include <peglib/rail.scad>;


tray_iw = 160;
tray_ow = tray_iw + (wall_thickness * 2);

tray_il = 30;
tray_ol = tray_il + (wall_thickness * 2);

tray_ih = base_size * 2;
tray_oh = tray_ih + (wall_thickness * 2);

wall_chamfer = wall_thickness / 4;




module tray() {
        // tray
    //
    union() {
        translate([0, tray_ol / 2, wall_thickness / 2]) {
            color("lightblue") {
                chamfered_box(dim = [tray_ow, tray_ol, wall_thickness], chamfer = wall_chamfer);
            }
            difference(){
                union(){
                    color("salmon") {
                        for(x = [-1, 1]) {
                            hull() {
                                for(y = [-1, 1]) {
                                    translate([x * ((tray_iw + wall_thickness) / 2), y * ((tray_il + wall_thickness) / 2), 0]) {
                                        echo(x, y);
                                        chamfered_box([wall_thickness, wall_thickness, tray_oh + (((-y + 1) / 2 ) * (tray_oh * 0.5 ))], chamfer = wall_chamfer, align = [0, 0, 1]);
                                    }                    
                                }
                            }
                        }
                    }
                }

            }
            
            color("orange") {
                for(y = [-1, 1]) {
                    hull() {
                        for(x = [-1, 1]) {
                            translate([x * ((tray_iw + wall_thickness) / 2), y * ((tray_il + wall_thickness) / 2), 0]) {
                                echo(x, y);
                                chamfered_box([wall_thickness, wall_thickness, tray_oh+ (((-y + 1) / 2 ) * (tray_oh * 0.5 ))], chamfer = wall_chamfer, align = [0, 0, 1]);
                            }                    
                        }
                    }
                }
            }

        }
    }
    

}

    // usb: 15mmx7mm
    //


// true
// false

do_rail = true;
do_tray = true; 

if (do_rail) {
    translate([0, -0.01, 0.01]) {
        color("lightgreen") {
            rail(3, copies = 2, chamfer = wall_chamfer);
        }
    }
}    
    
if (do_tray) { 
    tray();
    
}

//EOF
