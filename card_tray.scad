include <peglib/defaults.scad>;
include <moslib/libchamfer.scad>;

// additional space beyond the measurements of the card to allow easier insertion/removal of the card to/from the tray
breathing_room = 0.25;

tray_iw = 53 + breathing_room;
tray_ow = tray_iw + (wall_thickness * 2);

tray_il = 90 + breathing_room;
tray_ol = tray_il + (wall_thickness * 2);

tray_ih = 2 + breathing_room;
tray_oh = tray_ih + (wall_thickness * 1);

wall_chamfer = wall_thickness / 4;


module tray() {
    difference() {
        color("lightblue") {
          //  chamfered_box([tray_ow, tray_ol, wall_thickness], chamfer = wall_chamfer);
        }
        color("slategrey") {
            translate([0, tray_il / 2, 0]) {
          //  chamfered_box([tray_iw, base_size / 2, wall_thickness + 0.01], align = [0, -1, 0]);
            }
        }
    }

    translate([0, 0, 0]) {
        color("plum") {
            for(x = [-1, 1]) {
                translate([x * (tray_ow / 2), 0, 0]) {
                    chamfered_box([wall_thickness, tray_ol, wall_thickness], chamfer = wall_chamfer, align = [-x, 0, 0]);
                }
            }
            translate([0, (tray_ol / 2), 0]) {
                chamfered_box([tray_ow, wall_thickness, wall_thickness], chamfer = wall_chamfer, align = [0, -1, 0]);
            }
            translate([0, -(tray_ol / 2), 0]) {
                chamfered_box([tray_ow, tray_ol - base_size/2, wall_thickness], chamfer = wall_chamfer, align = [0, 1, 0]);
            }
        }
    }
        


    for(y = [-1, 1]) {
        translate([0, y * (tray_il / 2), 0]) {
            color("orange") {
                // end walls
                chamfered_box([tray_ow, wall_thickness, tray_oh], align = [0, y * 1, 1], chamfer = wall_chamfer);
            }
            color("red") {
                // end caps
                translate([0, 0, tray_oh]) {
                    chamfered_box([tray_ow, wall_thickness, wall_thickness], chamfer = wall_chamfer, align = [0, y * 0.5, -1]);
                }
            }
        }
    }
    
    for(x = [-1, 1]) {
        translate([x * (tray_iw / 2), 0]) {
            color("lavender") {
                // side walls
                chamfered_box([wall_thickness, tray_ol, tray_oh], align = [x * 1, 0, 1], chamfer = wall_chamfer);
            }
            color("peachpuff") {
                // side caps
                translate([0, 0, tray_oh]) {
                    chamfered_box([wall_thickness, tray_ol, wall_thickness], align = [x * 0.5, 0, -1], chamfer = wall_chamfer);
                }
            }
        }
    }

}



module reference_cube() {
    chamfered_box([tray_ow, tray_ol, tray_oh* 4], chamfer = wall_chamfer);
}


// true
// false

do_tray = true; 
do_reference_cube = false;

if (do_tray) { 
    tray();
}



if(do_reference_cube) {
    reference_cube();
}



//EOF
