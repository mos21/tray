include <peglib/defaults.scad>;
include <moslib/libchamfer.scad>;


fn = 250; // target is fn = 250


chamfered_cylinder(height = base_size, od = base_size, chamfer = base_size / 4, fn = fn);



//EOF
